DELIMITER //
CREATE PROCEDURE crearTablaRopa()
BEGIN
    CREATE TABLE ropas(
                          id int primary key auto_increment,
                          codigo varchar(30) unique,
                          tipo_prenda varchar(40),
                          marca varchar (40),
                          modelo varchar (40),
                          talla varchar(40),
                          material varchar(50),
                          precio double,
                          lugar_facturacion varchar(50),
                          fecha_facturacion timestamp);
END //