package com.alvarodiez;

import javax.swing.*;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Controlador implements ActionListener, TableModelListener {
    private Vista vista;
    private Modelo modelo;

    private enum tipoEstado {conectado, desconectado};
    private tipoEstado estado;

    /**
     * Constructor para la clase Controlador
     *
     * @param vista objeto de la clase Vista
     * @param modelo objeto de la clase Modelo
     */
    public Controlador(Vista vista, Modelo modelo) {
        this.modelo = modelo;
        this.vista = vista;
        estado = tipoEstado.desconectado;

        createTable();
        addActionListener(this);
        addTableModelListeners(this);
    }

    /**
     * Método para añadir funciones a los botones del formulario
     *
     * @param listener
     */
    private void addActionListener(ActionListener listener) {
        vista.itemConectar.addActionListener(listener);
        vista.itemCrearTabla.addActionListener(listener);
        vista.itemSalir.addActionListener(listener);
        vista.btnBuscar.addActionListener(listener);
        vista.btnEliminar.addActionListener(listener);
        vista.btnNuevo.addActionListener(listener);
        vista.btnOrdenMarca.addActionListener(listener);
        vista.btnTodo.addActionListener(listener);
        vista.btnOrdenAlfa.addActionListener(listener);
    }

    /**
     * Método para añadir funciones a la tabla de datos.
     *
     * @param listener
     */
    private void addTableModelListeners(TableModelListener listener){
        vista.dtm.addTableModelListener(listener);
    }

    /**
     * Método para crear la cabecera de la base de datos
     */
    private void createTable() {
        String[] headers={"id","codigo","tipo_prenda","marca","modelo","talla","material","precio","lugar_facturacion", "fecha_facturacion"};
        vista.dtm.setColumnIdentifiers(headers);
        vista.dtm1.setColumnIdentifiers(headers);


    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String c = e.getActionCommand();

        switch (c) {

            case "Conectar":
                if (estado==tipoEstado.desconectado)  {
                    try {
                        //Conectamos la base de datos
                        modelo.conectar();
                        vista.itemConectar.setText("Desconectar");
                        estado=tipoEstado.conectado;

                        //Mostramos las columnas de la tabla de datos
                        cargarFilas(modelo.getData());
                        cargarFilas1(modelo.getData());
                    } catch (SQLException e1) {
                        JOptionPane.showMessageDialog(null,"Error de conexión","Error",JOptionPane.ERROR_MESSAGE);
                        e1.printStackTrace();
                    }

                } else {
                    try {
                        //Desconectamos la base de datos
                        modelo.desconectar();
                        vista.itemConectar.setText("Conectar");
                        estado=tipoEstado.desconectado;
                        vista.lblAccion.setText("Desconectado");
                    } catch (SQLException e1) {
                        JOptionPane.showMessageDialog(null,"Error de desconexión","Error",JOptionPane.ERROR_MESSAGE);
                        e1.printStackTrace();
                    }


                }
                break;

            case "CrearTablaRopa":
                try {
                    //Llamamos al método que crea la tabla
                    modelo.crearTablaRopa();
                    vista.lblAccion.setText("Tabla ropas creada");
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }

                break;

            case "Salir":
                System.exit(0);
                break;

            case "Nuevo":
                try {
                    modelo.insertRopa(vista.tCodigo.getText(),vista.tTipoPrenda.getText(),vista.tMarca.getText(),
                            vista.tModelo.getText(), vista.tTalla.getText(), vista.tMaterial.getText(),
                            vista.tPrecio.getText(), vista.fechaDP.getDateTimePermissive(), vista.tLugar.getText());

                    //Cargamos las filas para mostrar los cambios
                    cargarFilas(modelo.getData());
                    cargarFilas1(modelo.getData());
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                break;

            case "Orden x fecha":
                try {
                    cargarFilas(modelo.ordenarFecha());
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }

                break;


            case "Mostrar todo":
                try {
                    cargarFilas(modelo.getData());
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                break;

            case "Buscar x marca":

                //Extraemos lo que hay en el textField tBuscar
                String ordenar=vista.tBuscar.getText();
                try {
                    ResultSet rs = modelo.ordenarMarca(ordenar);
                    cargarFilas1(rs);
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }

                break;

            case "Buscar codigo":
                String buscar=vista.tBuscar.getText();
                try {
                    ResultSet rs =modelo.buscarRopa(buscar);
                    cargarFilas1(rs);
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                break;

            case "Eliminar":
                try {
                    //Seleccionamos la fila y su respectivo id
                    int filaBorrar=vista.tabla.getSelectedRow();
                    int idBorrar= (Integer) vista.dtm.getValueAt(filaBorrar,0);
                    //Lo borramos y eliminamos la fila
                    modelo.elimRopa(idBorrar);
                    vista.dtm.removeRow(filaBorrar);
                    vista.lblAccion.setText("Fila Eliminada");
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                break;
        }
    }

    /**
     * Método para cargar las filas de la tabla de abajo
     *
     * @param resultSet
     *
     * @throws SQLException
     */
    private void cargarFilas1(ResultSet resultSet) throws SQLException {
        Object[] fila1 = new Object[10];
        vista.dtm1.setRowCount(0);

        while (resultSet.next()) {
            fila1[0]=resultSet.getObject(1);
            fila1[1]=resultSet.getObject(2);
            fila1[2]=resultSet.getObject(3);
            fila1[3]=resultSet.getObject(4);
            fila1[4]=resultSet.getObject(5);
            fila1[5]=resultSet.getObject(6);
            fila1[6]=resultSet.getObject(7);
            fila1[7]=resultSet.getObject(8);
            fila1[8]=resultSet.getObject(9);
            fila1[9]=resultSet.getObject(10);

            vista.dtm1.addRow(fila1);

        }

        if(resultSet.last()) {
            vista.lblAccion.setVisible(true);
            vista.lblAccion.setText(resultSet.getRow()+" filas cargadas");
        }

    }

    /**
     * Método para cargar las filas de la tabla de arriba
     *
     * @param resultSet
     *
     * @throws SQLException
     */
    private void cargarFilas(ResultSet resultSet) throws SQLException {
        Object[] fila = new Object[10];
        vista.dtm.setRowCount(0);

        while (resultSet.next()) {
            fila[0]=resultSet.getObject(1);
            fila[1]=resultSet.getObject(2);
            fila[2]=resultSet.getObject(3);
            fila[3]=resultSet.getObject(4);
            fila[4]=resultSet.getObject(5);
            fila[5]=resultSet.getObject(6);
            fila[6]=resultSet.getObject(7);
            fila[7]=resultSet.getObject(8);
            fila[8]=resultSet.getObject(9);
            fila[9]=resultSet.getObject(10);

            vista.dtm.addRow(fila);

        }

        if(resultSet.last()) {
            vista.lblAccion.setVisible(true);
            vista.lblAccion.setText(resultSet.getRow()+" filas cargadas");
        }

    }

    /**
     * Método para que se modifique la tabla cuando cambiemos algún valor
     *
     * @param e
     */
    @Override
    public void tableChanged(TableModelEvent e) {
        if (e.getType() == TableModelEvent.UPDATE) {
            System.out.println("Actualizada");
            int filaModif=e.getFirstRow();

            try {
                modelo.modifRopa((Integer)vista.dtm.getValueAt(filaModif, 0),(String)vista.dtm.getValueAt(filaModif, 1),
                                        (String)vista.dtm.getValueAt(filaModif, 2), (String)vista.dtm.getValueAt(filaModif, 3),
                                        (String)vista.dtm.getValueAt(filaModif, 4), (String)vista.dtm.getValueAt(filaModif, 5),
                                        (String)vista.dtm.getValueAt(filaModif, 6), (Double) vista.dtm.getValueAt(filaModif, 7),
                        (java.sql.Timestamp)vista.dtm.getValueAt(filaModif, 9), (String)vista.dtm.getValueAt(filaModif, 8));

                vista.lblAccion.setText("Columna actualizada");
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }
}