package com.alvarodiez;

import com.github.lgooddatepicker.components.DateTimePicker;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

public class Vista {
    JPanel panel;
    JFrame frame;

    JButton btnBuscar;
    JTextField tCodigo;
    JTextField tTipoPrenda;
    JTextField tMarca;
    JTextField tModelo;
    JTextField tTalla;
    JTextField tMaterial;
    JTextField tPrecio;
    JTextField tLugar;
    JTextField tBuscar;

    JButton btnNuevo;
    JButton btnEliminar;
    JButton btnOrdenMarca;
    JButton btnTodo;
    JButton btnOrdenAlfa;

    JLabel lblMarca;
    JLabel lblModelo;
    JLabel lblTalla;
    JLabel lblMaterial;
    JLabel lblPrecio;
    JLabel lblDiaFact;
    JLabel lblLugarFact;
    JLabel lblCodigo;
    JLabel lblTipoPrenda;
    JLabel lblAccion;

    JTable tabla;
    JTable tabla1;
    DateTimePicker fechaDP;


    DefaultTableModel dtm;
    DefaultTableModel dtm1;
    JMenuItem itemConectar;
    JMenuItem itemCrearTabla;
    JMenuItem itemSalir;

    /**
     * Constructor de la clase Vista
     */
    public Vista() {
        frame = new JFrame("Ropa");
        frame.setContentPane(panel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        dtm =new DefaultTableModel();
        dtm1 =new DefaultTableModel();
        tabla.setModel(dtm);
        tabla1.setModel(dtm1);
        crearMenu();
        frame.pack();
        frame.setVisible(true);
    }

    /**
     * Método para crear el botón de la barra del menú
     */
    private void crearMenu() {
        //Creamos las instancias
        JMenu menuArchivo = new JMenu("Archivo");
        JMenuBar barraMenu = new JMenuBar();

        //
        itemConectar = new JMenuItem("Conectar");
        itemConectar.setActionCommand("Conectar");
        itemCrearTabla = new JMenuItem("Crear tabla ROPA");
        itemCrearTabla.setActionCommand("CrearTablaROPA");
        itemSalir = new JMenuItem("Salir");
        itemSalir.setActionCommand("Salir");

        menuArchivo.add(itemConectar);
        menuArchivo.add(itemCrearTabla);
        menuArchivo.add(itemSalir);
        barraMenu.add(menuArchivo);

        frame.setJMenuBar(barraMenu);

    }


}
