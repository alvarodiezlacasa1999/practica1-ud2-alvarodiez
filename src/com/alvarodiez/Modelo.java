package com.alvarodiez;

import java.sql.*;
import java.time.LocalDateTime;

public class Modelo {
    private Connection conex;

    /**
     * Método que sirve para conectar con la base de datos
     * y crear la tabla.
     *
     * @throws SQLException Excepción que se lanza al conectarse con una base de datos
     */
    public void crearTablaRopa() throws SQLException {
        conex=null;
        conex= DriverManager.getConnection("jdbc:mysql://localhost:3306/ropa", "root", "");

        String sentencia = "call crearTablaRopa()";
        CallableStatement procedimiento=null;
        procedimiento=conex.prepareCall(sentencia);
        procedimiento.execute();
    }

    /**
     * Método para conectarse a la base de datos que lanza
     * una excepción para controlar un posible error de la bbdd
     *
     * @throws SQLException
     */
    public void conectar() throws SQLException {
        conex=null;
        conex= DriverManager.getConnection("jdbc:mysql://localhost:3306/ropa", "root", "");
    }

    /**
     * Método para desconectarse a la base de datos que lanza
     * una excepción para controlar un posible error de la bbdd
     *
     * @throws SQLException
     */
    public void desconectar() throws SQLException {
        conex.close();
        conex=null;
    }

    /**
     * Método para realizar una consulta a nuestra bbdd de toda la ropa que
     * tenemos registrada.
     * Nos devuelve el dato de la consulta.
     *
     * @return resultado
     * @throws SQLException
     */
    public ResultSet getData() throws SQLException {
        if (conex==null) {
            return null;
        }
        if (conex.isClosed()) {
            return null;
        }

        //Creamos la consulta para la base
        String consulta ="SELECT * FROM ropas";
        PreparedStatement sentencia = null;
        sentencia=conex.prepareStatement(consulta);
        ResultSet resultado=sentencia.executeQuery();
        return resultado;
    }

    /**
     * Método para ordenar la tabla por marca de ropa.
     *
     * @param marca marca de la prenda
     *
     * @return resultado
     * @throws SQLException
     */
    public ResultSet ordenarMarca(String marca) throws SQLException {
        if (conex==null) {
            return null;
        }
        if (conex.isClosed()) {
            return null;
        }

        String consulta ="SELECT * FROM ropas WHERE marca=?";
        PreparedStatement sentencia = null;

        sentencia=conex.prepareStatement(consulta);
        sentencia.setString(1,marca);

        ResultSet resultado=sentencia.executeQuery();
        return resultado;

    }

    /**
     * Método para ordenar la tabla por fecha de facturación,
     * desde la fecha más proxima hasta la más lejana.
     *
     * @return resultado
     * @throws SQLException
     */
    public ResultSet ordenarFecha() throws SQLException {
        if (conex==null) {
            return null;
        }
        if (conex.isClosed()) {
            return null;
        }

        String consulta ="SELECT * FROM ropas ORDER BY fecha_facturacion ASC";
        PreparedStatement sentencia = null;
        sentencia=conex.prepareStatement(consulta);
        ResultSet resultado=sentencia.executeQuery();
        return resultado;
    }

    /**
     * Método para introducir en la base de datos los elementos
     * que hayamos rellenado en el formulario.
     *
     * @param codigo código de la ropa
     * @param tipo_prenda tipo de prenda
     * @param marca marca del producto
     * @param modelo modelo del producto
     * @param talla talla de la prenda
     * @param material material del que está hecha la prenda
     * @param precio precio de la prenda
     * @param fecha_facturacion fecha en la que se envia
     * @param lugar_facturacion lugar al que se envía
     *
     * @return numRegistros número de registros
     * @throws SQLException
     */
    public int insertRopa(String codigo, String tipo_prenda, String marca, String modelo, String talla, String material,
                          String precio, LocalDateTime fecha_facturacion, String lugar_facturacion) throws SQLException {
        if (conex==null)
            return -1;
        if (conex.isClosed())
            return -2;

        String consulta="INSERT INTO ropas(codigo, tipo_prenda, marca, modelo, talla," +
                "material, precio, fecha_facturacion, lugar_facturacion) VALUES (?,?,?,?,?,?,?,?,?)";

        PreparedStatement sentencia = null;

        sentencia=conex.prepareStatement(consulta);
        sentencia.setString(1,codigo);
        sentencia.setString(2,tipo_prenda);
        sentencia.setString(3,marca);
        sentencia.setString(4,modelo);
        sentencia.setString(5,talla);
        sentencia.setString(6,material);
        sentencia.setDouble(7, Double.parseDouble(precio));
        sentencia.setTimestamp(8, Timestamp.valueOf(fecha_facturacion));
        sentencia.setString(9,lugar_facturacion);

        int numRegistros=sentencia.executeUpdate();

        if (sentencia!=null) {
            sentencia.close();
        }

        return numRegistros;
    }

    /**
     * Método para eliminar un artículo según su id
     *
     * @param id número de identificación del producto
     *
     * @return resultado
     * @throws SQLException
     */
    public int elimRopa(int id) throws SQLException {
        if (conex==null)
            return -1;
        if (conex.isClosed())
            return -2;


        String consulta="DELETE FROM ropas WHERE id=?";
        PreparedStatement sentencia=null;

        sentencia=conex.prepareStatement(consulta);
        sentencia.setInt(1,id);

        int resultado=sentencia.executeUpdate();

        if (sentencia!=null) {
            sentencia.close();
        }

        return resultado;
    }

    /**
     * Método para modificar algún campo de la tabla
     *
     * @param id id de la ropa
     * @param codigo código de la ropa
     * @param tipo_prenda tipo de prenda
     * @param marca marca del producto
     * @param modelo modelo del producto
     * @param talla talla de la prenda
     * @param material material del que está hecha la prenda
     * @param precio precio de la prenda
     * @param fecha_facturacion fecha en la que se envia
     * @param lugar_facturacion lugar al que se envía
     *
     * @return resultado
     * @throws SQLException
     */
    public int modifRopa (int id, String codigo, String tipo_prenda, String marca, String modelo, String talla,
                                  String material, double precio, Timestamp fecha_facturacion, String lugar_facturacion) throws SQLException {
        if (conex==null)
            return -1;
        if (conex.isClosed())
            return -2;

        String consulta="UPDATE ropas SET codigo=?, tipo_prenda=?, marca=?, modelo=?, talla=?," +
                "material=?, precio=?, fecha_facturacion=?, lugar_facturacion=? WHERE id=?";

        PreparedStatement sentencia=null;

        sentencia=conex.prepareStatement(consulta);

        sentencia.setString(1,codigo);
        sentencia.setString(2,tipo_prenda);
        sentencia.setString(3,marca);
        sentencia.setString(4,modelo);
        sentencia.setString(5,talla);
        sentencia.setString(6,material);
        sentencia.setDouble(7,precio);
        sentencia.setTimestamp(8,fecha_facturacion);
        sentencia.setString(9,lugar_facturacion);
        sentencia.setInt(10,id);

        int resultado=sentencia.executeUpdate();

        if (sentencia!=null) {
            sentencia.close();
        }

        return resultado;
    }

    /**
     * Método para buscar una prenda por su código
     *
     * @param codigo código de la prenda a buscar
     *
     * @return resultado
     * @throws SQLException
     */
    public ResultSet buscarRopa(String codigo) throws SQLException {
        if( conex == null)
            return null;

        if( conex.isClosed())
            return null;

            String consulta = "SELECT * FROM ropas WHERE codigo=?";
            PreparedStatement sentencia = null;
            sentencia = conex.prepareStatement(consulta);
            sentencia.setString(1, String.valueOf(codigo));
            ResultSet resultado = sentencia.executeQuery();
            return resultado;
    }
}

