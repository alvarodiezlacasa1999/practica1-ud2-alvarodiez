package com.alvarodiez;

public class Principal {
    /**
     * Clase principal donde se ejecutarán las 3 partes del programa
     *
     * @param args
     */
    public static void main(String args[]) {
        Vista vista = new Vista();
        Modelo modelo = new Modelo();
        Controlador controlador = new Controlador(vista, modelo);
    }
}
